Red [
	Description: "Pie style with panel and extendable actors"
	Date: 9-Nov-2020
	Updated: 14-Nov-2020
	Author: "Toomas Vooglaid"
	EditBy: "Brock Kalef"
]

extend system/view/VID/styles [
	radar: [
		template: [
			type: 'panel
			actors: [
				on-create: function [face][
					margin: any [all [face/extra face/extra/margin] 0x0]
					padding: any [all [face/extra face/extra/padding] 10x10]
					size: face/size - margin
					;radius: face/size - (2 * padding) / 2 	;for consistent pie chart radius
					radius: -1 	; ignoring temporarily -      default value, will be replaced when in while loop for each record
					list: face/data
					if all [list: face/data not empty? list] [
						total: sum collect [forall list [if number? list/1 [keep list/1]]]
					]
					color: any [
						all [face/extra face/extra/color] 
						[brick aqua papaya mint orange leaf khaki olive 
							sienna tanned sky rebolor gold pink teal]
					]
					start: -90
					;sweep-sum: 0
					fill-image: load %burst-small-trans.png 
					template: [at 0x0 box (size) draw [
							translate (size / 2)
							;translate (size / 2) fill-pen bitmap (fill-image)
							arc 0x0 100x100 (start) (sweep) closed
							fill-pen (color/1) 
							;fill-pen bitmap (fill-image) 0x0 100x100
							arc 0x0 (as-pair list/2 list/2) (start) (sweep) closed
							fill-pen off
							rotate (2 * start + sweep / 2) 0x0 
							;text (as-pair radius/x + 10 -5) (text)
							text 110x-5 (list/1)
						]
					]
					pane: compose/deep [origin (margin)
						at 0x0 box (size) draw[
							translate (size / 2)
							circle 0x0 10
							circle 0x0 20
							circle 0x0 30
							circle 0x0 40
							circle 0x0 50
							circle 0x0 60
							circle 0x0 70
							circle 0x0 80
							circle 0x0 90
							circle 0x0 100
						]
					]
					sectors: clear []
					remainder: 0
					if all [list not empty? list] [
						poles: divide (length? list) 2
						if not 0 = mod 360 poles [remainder: mod 360 poles]
						;foreach [text num] list [
						while [not tail? list][
							print [first list tab second list tab tail? list]
							;sweep: round/to num * 360.0 / total 1
							sweep: round/to (360 / poles) 1
							if 0 < remainder [sweep: sweep + 1  remainder: remainder - 1]	; alternate between addingg 1 extra sweep degree and not to handle the remainder
							append sectors compose/deep template
							probe sectors
							;if face/extra [
							;	if pen: face/extra/pen [insert last sectors reduce ['pen pen]]
							;	if line-width: face/extra/line-width [insert last sectors reduce ['line-width line-width]]
							;	if actors: face/extra/actors [append sectors copy/deep actors]
							;]
							color: any [all [tail? color head color] next color]
							start: start + sweep
							;sweep-sum: sweep-sum + sweep
							
							list: skip list 2
						]
						;if sweep-sum < 360 [
						;	change last-sweep: at sectors/draw 9 last-sweep/1 + 360 - sweep-sum
						;]
						;probe sectors
						append pane skip sectors 2
						face/pane: layout/only pane
						;probe pane
					]
				]
			]
		]
		init: [unless face/size > 0x0 [face/size: 250x250]]
	]
]
