# Radar-style

Replication and Edit of Toomas Vooglaid's work on pie-style.red, but modified to work as a radar graph (polar graph / spider graph) instead.  Currently there are some hard-coded values that need to be removed, and the fill-pen bitmap is not working as expected.

A sample to execute is as follows;
```
view [
	radar 300x300 beige data ["First" 58 "Second" 78 "Third" 89 "Fourth" 69 "Fifth" 85 "Sixth" 95 "Seventh" 74] 
]
```
