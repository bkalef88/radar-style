; rotated text - scaled image still incorrect
Red [Needs: View]
context [
    fill-image: load %burst-small-trans.png 
    tx: make-face 'text
    txt-pos: function [rad ang text][
        txt-size: size-text/with tx text
        x: rad * cosine ang
        y: rad * sine ang
        case [
            ang < -45 [y: y - 15]
            ang > 225 [x: x - txt-size/x y: y - 15]
            all [ang > 70 ang < 110][x: x - (txt-size/x / 2)]
            ang > 90 [x: x - txt-size/x]
        ]
        as-pair x y
    ]
	
	pole-pos: function [rad2 ang2][
		x: rad2 * cosine ang2
		y: rad2 * sine ang2
		as-pair x y
	]
	
    extend system/view/VID/styles [
        radar: [
            template: [
                type: 'panel
                actors: [
                    on-create: function [face][
                        face/visible?: no
                        margin: any [all [face/extra face/extra/margin] 0x0]
                        padding: any [all [face/extra face/extra/padding] 10x10]
						pad2: padding * 2
                        size: face/size - (2 * margin)
						?? size
                        radar-size: size - (2 * padding)
						?? radar-size
                        radius: radar-size/x / 2 + 10
						?? radius
                        list: face/data
                        start: -90
                        template: [
                            at (padding) box (size - padding)  draw [
                                ;translate (sz: size / 2)
								[
									pen gray
									;rotate ang center-pt [draw block to happen based on rotation]
                                    ;rotate (start) ((face/size - pad2) / 2) [line ((face/size - pad2) / 2) (as-pair radar-size/x / 2 - 12 radar-size/y / 2 - 12)]	; division lines of radar graph for each segment
									rotate (start) (face/size - pad2 / 2 ) [line (face/size - pad2 / 2) (pole-pos radar-size/x / 2 start)]
                                ]
                                ;fill-pen pattern (s: fill-image/size) [image fill-image]
                                fill-pen bitmap fill-image clamp
                                ;translate (sz: size / 2)
								[		; draw arc sectors and the associated text
                                    arc (face/size - pad2 / 2) ((to-pair list/2) * radar-size / 200) (start) (sweep) closed
                                    fill-pen off
									translate (sz: size - pad2 / 2)
                                    text (txt-pos radius ang list/1) (rejoin [list/1 newline list/2])
                                ]
                            ]
                        ]
                        step: radar-size/x / 20
                        rad: 0
                        pane: compose/deep [			; pane to record draw background grid area
                            origin (margin)
                            at 0x0 box (size) draw [
                                translate (size / 2)
                                pen gray 
                            ]
                        ]
                        loop 10 [repend last pane ['circle 0x0 rad: rad + step]]
                        sectors: clear []
                        remainder: 0
                        if all [list not empty? list] [
                            poles: (length? list) / 2				; determine number of sectors to draw in the graph
                            if not 0 = mod 360 poles [remainder: mod 360 poles]
                            while [not tail? list][
                                sweep: round/to (360 / poles) 1
                                if 0 < remainder [sweep: sweep + 1  remainder: remainder - 1]    ; adding 1 extra sweep degree for each remainder count
                                ang: 2 * start + sweep / 2
                                append sectors compose/deep template
                                if face/extra [
                                    if pen: face/extra/pen [insert last sectors reduce ['pen pen]]
                                    if line-width: face/extra/line-width [insert last sectors reduce ['line-width line-width]]
                                    if actors: face/extra/actors [append sectors copy/deep actors]
                                ]
                                start: start + sweep
                                list: skip list 2
                            ]
                            append pane skip sectors 2
                            face/pane: layout/only pane
                        ]
                        face/visible?: yes
                    ]
                ]
            ]
            init: [unless face/size > 0x0 [face/size: 250x250]]
        ]
    ]
     view/flags [on-resizing [r/size: to-pair min face/size/x face/size/y r/actors/on-create r] r: radar 540x540 beige data ["First" 58 "Second" 78 "Third" 89 "Fourth" 69 "Fifth" 85 "Sixth" 95 "Seventh" 74 "Eighth" 100 "Ninth" 90] extra [padding: 80x80]] 'resize
]